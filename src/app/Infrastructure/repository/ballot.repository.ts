import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppSettings} from '../../Setting/app.setting';

@Injectable()
export abstract class IBallotRepository {
  abstract GetBallotAll(): Promise<any>;

  abstract CreateBallot(Ballot: any): Promise<any>;

  abstract UpdateBallot(Ballot: any): Promise<any>;

  abstract deleteBallot(Ballot: any): Promise<any>;
}

@Injectable()
export class BallotRepository implements IBallotRepository {
  private constants: AppSettings;
  private urlBallot: string;

  constructor(private http: HttpClient) {
    this.constants = new AppSettings();
    this.urlBallot = `${this.constants.SiteUrl}/ballot`;
  }

  GetBallotAll(): Promise<any> {

    return this.http.get(this.urlBallot).toPromise().then(response => {
      return response;
    });
  }

  CreateBallot(Ballot: any): Promise<any> {
    return this.http.post(this.urlBallot, Ballot).toPromise().then(response => {
      return response;
    });
  }

  UpdateBallot(Ballot: any): Promise<any> {
    return this.http.put(`${this.urlBallot}/${Ballot.id}`, Ballot).toPromise().then(response => {
      return response;
    });
  }

  deleteBallot(Ballot: any): Promise<any> {
    return this.http.delete(`${this.urlBallot}/${Ballot.id}`).toPromise().then(response => {
      return response;
    });
  }

}
