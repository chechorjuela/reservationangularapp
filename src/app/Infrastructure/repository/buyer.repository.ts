import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppSettings} from '../../Setting/app.setting';

@Injectable()
export abstract class IBuyerRepository {
  abstract GetBuyerAll(): Promise<any>;

  abstract CreateBuyer(buyer: any): Promise<any>;

  abstract UpdateBuyer(buyer: any): Promise<any>;

  abstract deleteBuyer(buyer: any): Promise<any>;
}

@Injectable()
export class BuyerRepository implements IBuyerRepository {
  private constants: AppSettings;
  private urlBuyer: string;

  constructor(private http: HttpClient) {
    this.constants = new AppSettings();
    this.urlBuyer = `${this.constants.SiteUrl}/buyer`;
  }

  GetBuyerAll(): Promise<any> {

    return this.http.get(this.urlBuyer).toPromise().then(response => {
      return response;
    });
  }

  CreateBuyer(buyer: any): Promise<any> {
    return this.http.post(this.urlBuyer, buyer).toPromise().then(response => {
      return response;
    });
  }

  UpdateBuyer(buyer: any): Promise<any> {
    return this.http.put(`${this.constants.SiteUrl}/buyer/${buyer.id}`, buyer).toPromise().then(response => {
      return response;
    });
  }

  deleteBuyer(buyer: any): Promise<any> {
    return this.http.delete(`${this.constants.SiteUrl}/buyer/${buyer.id}`).toPromise().then(response => {
      return response;
    });
  }

}
