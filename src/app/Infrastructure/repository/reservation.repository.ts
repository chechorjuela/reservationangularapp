import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppSettings} from '../../Setting/app.setting';

@Injectable()
export abstract class IReservationRepository {
  abstract GetReservationAll(): Promise<any>;

  abstract CreateReservation(Reservation: any): Promise<any>;

  abstract UpdateReservation(Reservation: any): Promise<any>;

  abstract deleteReservation(Reservation: any): Promise<any>;
}

@Injectable()
export class ReservationRepository implements IReservationRepository {
  private constants: AppSettings;
  private urlReservation: string;

  constructor(private http: HttpClient) {
    this.constants = new AppSettings();
    this.urlReservation = `${this.constants.SiteUrl}/reservation`;
  }

  GetReservationAll(): Promise<any> {

    return this.http.get(this.urlReservation).toPromise().then(response => {
      return response;
    });
  }

  CreateReservation(Reservation: any): Promise<any> {
    return this.http.post(this.urlReservation, Reservation).toPromise().then(response => {
      return response;
    });
  }

  UpdateReservation(Reservation: any): Promise<any> {
    return this.http.put(`${this.urlReservation}/${Reservation.id}`, Reservation).toPromise().then(response => {
      return response;
    });
  }

  deleteReservation(Reservation: any): Promise<any> {
    return this.http.delete(`${this.urlReservation}/${Reservation.id}`).toPromise().then(response => {
      return response;
    });
  }

}
