import {Component, Inject, OnInit} from '@angular/core';
import {BuyerService} from '../../../../Domain/Services/buyer.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {DialogData} from '../../modal-buyer/modalbuyer.component';
import {BuyerRepository} from '../../../../Infrastructure/repository/buyer.repository';
import {BallotService} from '../../../../Domain/Services/ballot.service';
import {BallotRepository} from '../../../../Infrastructure/repository/ballot.repository';

@Component({
  selector: 'app-confirmdialog',
  templateUrl: './confirmdialog.component.html',
  styleUrls: ['./confirmdialog.component.sass'],
  providers: [BallotService, BallotRepository]
})
export class ComfirmodalBallotComponent implements OnInit {

  constructor(private ballotService: BallotService, public dialogRef: MatDialogRef<ComfirmodalBallotComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  ngOnInit(): void {
  }

  onDelete(): void {
    this.ballotService.deleteBallot(this.data).then(response => {
      if (response.data) {
        this.dialogRef.close();
      }
    });
  }
}
