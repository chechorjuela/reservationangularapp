import {Component, Inject, OnInit} from '@angular/core';
import {BuyerService} from '../../../../Domain/Services/buyer.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {DialogData} from '../../modal-buyer/modalbuyer.component';
import {BuyerRepository} from '../../../../Infrastructure/repository/buyer.repository';

@Component({
  selector: 'app-confirmdialog',
  templateUrl: './confirmdialog.component.html',
  styleUrls: ['./confirmdialog.component.sass'],
  providers: [BuyerService, BuyerRepository]
})
export class ConfirmdialogComponent implements OnInit {

  constructor(private buyerService: BuyerService, public dialogRef: MatDialogRef<ConfirmdialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  ngOnInit(): void {
  }

  onDelete(): void {
    this.buyerService.deleteBuyer(this.data).then(response => {
      if(response.data){
        this.dialogRef.close();
      }
    });
  }
}
