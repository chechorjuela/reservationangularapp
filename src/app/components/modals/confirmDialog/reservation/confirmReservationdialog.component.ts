import {Component, Inject, OnInit} from '@angular/core';
import {BuyerService} from '../../../../Domain/Services/buyer.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {DialogData} from '../../modal-buyer/modalbuyer.component';
import {BuyerRepository} from '../../../../Infrastructure/repository/buyer.repository';
import {ReservationService} from '../../../../Domain/Services/reservation.service';
import {ReservationRepository} from '../../../../Infrastructure/repository/reservation.repository';

@Component({
  selector: 'app-confirmdialog',
  templateUrl: './confirmReservationdialog.component.html',
  styleUrls: ['./confirmReservationdialog.component.sass'],
  providers: [ReservationService, ReservationRepository]
})
export class ConfirmReservationdialogComponent implements OnInit {
  constructor(private reservationService: ReservationService, public dialogRef: MatDialogRef<ConfirmReservationdialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  ngOnInit(): void {
  }

  onDelete(): void {
    this.reservationService.deleteReservation(this.data).then(response => {
      if (response.data) {
        this.dialogRef.close();
      }
    });
  }
}
