import {Component, OnInit, Inject, Output, EventEmitter} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {BuyerService} from '../../../Domain/Services/buyer.service';
import {BuyerRepository} from '../../../Infrastructure/repository/buyer.repository';

export interface DialogData {
  id: number;
  firstname: string;
  lastname: string;
  email: string;
}

@Component({
  selector: 'app-modal-buyer',
  templateUrl: './modalbuyer.component.html',
  styleUrls: ['./modal-buyer.component.sass'],
  providers: [BuyerService, BuyerRepository]
})
export class ModalbuyerComponent implements OnInit {


  public formGroup: FormGroup = new FormGroup({
    firstname: new FormControl(''),
    lastName: new FormControl(''),
  });

  constructor(private formBuilder: FormBuilder, public dialogRef: MatDialogRef<ModalbuyerComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData, private buyerservice: BuyerService) {

  }

  ngOnInit(): void {
    this.buildForm();
  }

  private buildForm(): void {
    if (this.data != null) {
      this.formGroup = this.formBuilder.group({
        id: this.data.id != null ? this.data.id : '',
        firstname: this.data.firstname != null ? this.data.firstname : '',
        lastname: this.data.lastname != null ? this.data.lastname : '',
        email: this.data.email != null ? this.data.email : '',
      });
    }

  }

  onSubmit(): void {
    if (this.formGroup.value.id <= 0) {
      this.buyerservice.saveBuyer(this.formGroup.value).then(response => {
        this.dialogRef.close();
      });
    } else {
      this.buyerservice.updateBuyer(this.formGroup.value).then(response => {
        this.dialogRef.close();
      });
    }
  }

}
