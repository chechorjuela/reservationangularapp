import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {BallotService} from '../../../Domain/Services/ballot.service';
import {BallotRepository} from '../../../Infrastructure/repository/ballot.repository';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {DatePipe} from '@angular/common';

export interface DialogData {
  id: number;
  name: string;
  amount: string;
  available: string;
  date_expiration: Date;

}

@Component({
  selector: 'app-modalballot',
  templateUrl: './modalballot.component.html',
  styleUrls: ['./modalballot.component.sass'],
  providers: [BallotService, BallotRepository]
})
export class ModalballotComponent implements OnInit {
  public formGroup: FormGroup = new FormGroup({
    name: new FormControl(''),
    date_expiration: new FormControl(''),
    amount: new FormControl(''),
    available: new FormControl(''),
  });

  constructor(private formBuilder: FormBuilder, public dialogRef: MatDialogRef<ModalballotComponent>,
              private ballotService: BallotService, @Inject(MAT_DIALOG_DATA) public data: DialogData, public datepipe: DatePipe) {
  }

  ngOnInit(): void {
    this.buildForm();
  }

  private buildForm(): void {
    if (this.data != null) {
      this.formGroup = this.formBuilder.group({
        id: this.data.id != null ? this.data.id : '',
        name: this.data.name != null ? this.data.name : '',
        amount: this.data.amount != null ? this.data.amount : '',
        available: this.data.available != null ? this.data.available : '',
        date_expiration: this.data.available != null ? this.data.date_expiration : '',
      });
    }

  }

  onSubmit(): void {
    if (this.formGroup.value.id <= 0) {
      this.formGroup.value.date_expiration = this.datepipe.transform(this.formGroup.value.date_expiration, 'yyyy-MM-dd');
      this.ballotService.saveBallot(this.formGroup.value).then(response => {
        this.dialogRef.close();
      });
    } else {
      this.formGroup.value.date_expiration = this.datepipe.transform(this.formGroup.value.date_expiration, 'yyyy-MM-dd');
      this.ballotService.updateBallot(this.formGroup.value).then(response => {
        this.dialogRef.close();
      });
    }
  }
}
