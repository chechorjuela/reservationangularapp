import {Component, Inject, OnInit} from '@angular/core';
import {ReservationService} from '../../../Domain/Services/reservation.service';
import {ReservationRepository} from '../../../Infrastructure/repository/reservation.repository';
import {BuyerService} from '../../../Domain/Services/buyer.service';
import {BuyerRepository} from '../../../Infrastructure/repository/buyer.repository';
import {BallotService} from '../../../Domain/Services/ballot.service';
import {BallotRepository} from '../../../Infrastructure/repository/ballot.repository';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';

export interface DialogData {
  id: number;
  buyer_id: number;
  ballot_id: string;
  amount: number;
}

export interface SelectData {
  id: string;
  firstname: string;
  lastname: string;
}

export interface SelectDataBallot {
  id: string;
  name: string;
}

@Component({
  selector: 'app-modalreservation',
  templateUrl: './modalreservation.component.html',
  styleUrls: ['./modalreservation.component.sass'],
  providers: [ReservationService, ReservationRepository, BuyerService, BuyerRepository, BallotService, BallotRepository]
})
export class ModalreservationComponent implements OnInit {
  public buyers: any;
  public ballots: any;
  public selectBuyer: SelectData = {
    id: '0',
    firstname: '',
    lastname: ''
  };
  public selectBallot: SelectDataBallot = {
    id: '0',
    name: '',
  };
  public formGroup: FormGroup = new FormGroup({
    buyer_id: new FormControl(''),
    ballot_id: new FormControl(''),
    amount: new FormControl(''),
  });

  constructor(private formBuilder: FormBuilder, private reservationService: ReservationService, private ballotService: BallotService,
              private buyerService: BuyerService, public dialogRef: MatDialogRef<ModalreservationComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  ngOnInit(): void {
    this.buyerService.getBuyerAll().then(response => {
      this.buyers = response.data;
      if (this.data.id > 0) {
        this.selectBuyer = this.buyers.find((b: any) => this.data.buyer_id === b.id);
      }
    });
    this.ballotService.getBallotAll().then(response => {
      this.ballots = response.data;
      if (this.data.id > 0) {
        this.selectBallot = this.ballots.find((b: any) => this.data.ballot_id === b.id);

      }
    });
    this.buildForm();
  }

  private buildForm(): void {
    if (this.data != null) {
      this.formGroup = this.formBuilder.group({
        id: this.data.id != null ? this.data.id : '',
        buyer_id: this.data.buyer_id != null ? this.data.buyer_id : '',
        ballot_id: this.data.ballot_id != null ? this.data.ballot_id : '',
        amount: this.data.amount != null ? this.data.amount : '',
      });
    }

  }

  onSubmit(): void {
    if (this.formGroup.value.id <= 0) {
      this.reservationService.saveReservation(this.formGroup.value).then(response => {
        this.dialogRef.close();
      });
    } else {
      this.reservationService.updateReservation(this.formGroup.value).then(response => {
        this.dialogRef.close();
      });
    }
  }
}
