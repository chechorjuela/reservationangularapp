import {Component, OnInit} from '@angular/core';
import {BuyerService} from '../../Domain/Services/buyer.service';
import {MatDialog} from '@angular/material/dialog';
import {ModalbuyerComponent} from '../modals/modal-buyer/modalbuyer.component';
import {ConfirmdialogComponent} from '../modals/confirmDialog/Buyer/confirmdialog.component';
import {ReservationService} from '../../Domain/Services/reservation.service';
import {ReservationRepository} from '../../Infrastructure/repository/reservation.repository';
import {ModalreservationComponent} from '../modals/modalreservation/modalreservation.component';
import {ConfirmReservationdialogComponent} from '../modals/confirmDialog/reservation/confirmReservationdialog.component';

@Component({
  selector: 'app-reservationcomponent.component',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.sass'],
  providers: [ReservationService, ReservationRepository]
})
export class ReservationComponent implements OnInit {

  public reservations: any;
  displayedColumns: string[] = ['id', 'ballot_id', 'buyer_id', 'amount', 'Actions'];

  constructor(
    private reservationService: ReservationService,
    private dialog: MatDialog
  ) {
  }


  ngOnInit(): void {
    this.loadingPage();

  }

  loadingPage(): void {
    this.reservationService.getReservationAll().then(response => {
      if (response.Header.status === 'success') {
        this.reservations = response.data;
      }
    });
  }

  changeDialog(): void {
    const dialogConfig = this.dialog.open(ModalreservationComponent, {
      data: {
        id: 0,
        buyer_id: '',
        ballot_id: '',
      }
    });
    dialogConfig.afterClosed().subscribe(result => {
      this.loadingPage();
    });
  }

  onSave(buyer: any): any {

  }


  onEdit(reservation: any): void {
    const dialogConfig = this.dialog.open(ModalreservationComponent, {
      data: reservation,
    });
    dialogConfig.afterClosed().subscribe(result => {
      this.loadingPage();
    });

  }

  onDelete(reservation: any): void {
    const deleteDialog = this.dialog.open(ConfirmReservationdialogComponent, {
      data: reservation
    });
    deleteDialog.afterClosed().subscribe(result => {
      this.loadingPage();
    });
  }
}
