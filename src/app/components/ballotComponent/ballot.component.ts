import {Component, OnInit} from '@angular/core';
import {BallotService} from '../../Domain/Services/ballot.service';
import {BallotRepository} from '../../Infrastructure/repository/ballot.repository';
import {MatDialog} from '@angular/material/dialog';
import {ModalballotComponent} from '../modals/modalballot/modalballot.component';
import {ComfirmodalBallotComponent} from '../modals/confirmDialog/ballot/confirmdialog.component';

@Component({
  selector: 'app-ballot.component',
  templateUrl: './ballot.component.html',
  styleUrls: ['./ballot.component.sass'],
  providers: [BallotService, BallotRepository]
})
export class BallotComponent implements OnInit {
  public ballots: any;
  displayedColumns: string[] = ['id', 'name', 'date_expiration', 'amount', 'available', 'Actions'];

  constructor(private ballotService: BallotService, private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.loadingPage();
  }

  loadingPage(): void {
    this.ballotService.getBallotAll().then(response => {
      if (response.Header.status === 'success') {
        this.ballots = response.data;
      }
    });
  }

  changeDialog(): void {
    const dialogConfig = this.dialog.open(ModalballotComponent, {
      data: {
        id: 0,
        date_expiration: '',
        name: '',
        amount: '',
        available: '',
      },
    });
    dialogConfig.afterClosed().subscribe(result => {
      this.loadingPage();
    });
  }

  onSave(buyer: any): any {

  }


  onEdit(buyer: any): void {
    const dialogConfig = this.dialog.open(ModalballotComponent, {
      data: buyer,
    });
    dialogConfig.afterClosed().subscribe(result => {
      this.loadingPage();
    });

  }

  onDelete(ballot: any): void {
    const deleteDialog = this.dialog.open(ComfirmodalBallotComponent, {
      data: ballot,
    });
    deleteDialog.afterClosed().subscribe(result => {
      this.loadingPage();
    });
  }

}
