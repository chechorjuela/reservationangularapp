import {Component, OnInit} from '@angular/core';
import {BuyerService} from '../../Domain/Services/buyer.service';
import {BuyerRepository} from '../../Infrastructure/repository/buyer.repository';
import {MatDialog, MatDialogModule} from '@angular/material/dialog';
import {ModalbuyerComponent} from '../modals/modal-buyer/modalbuyer.component';
import {ConfirmdialogComponent} from '../modals/confirmDialog/Buyer/confirmdialog.component';


@Component({
  selector: 'app-buyer.component',
  templateUrl: './buyer.component.html',
  styleUrls: ['./buyer.component.sass'],
  providers: [BuyerService, BuyerRepository]
})
export class BuyerComponent implements OnInit {

  public buyers: any;
  displayedColumns: string[] = ['id', 'firstname', 'lastname', 'email', 'Actions'];

  constructor(
    private buyerService: BuyerService,
    private dialog: MatDialog
  ) {
  }



  ngOnInit(): void {
    this.loadingPage();

  }

  loadingPage(): void {
    this.buyerService.getBuyerAll().then(response => {
      if (response.Header.status === 'success') {
        this.buyers = response.data;
      }
    });
  }

  changeDialog(): void {
    const dialogConfig = this.dialog.open(ModalbuyerComponent, {
      data: {
        id: 0,
        firstname: '',
        lastname: '',
        email: ''
      },
    });
    dialogConfig.afterClosed().subscribe(result => {
      this.loadingPage();
    });
  }

  onSave(buyer: any): any {

  }


  onEdit(buyer: any): void {
    const dialogConfig = this.dialog.open(ModalbuyerComponent, {
      data: buyer,
    });
    dialogConfig.afterClosed().subscribe(result => {
      this.loadingPage();
    });

  }

  onDelete(buyer: any): void {
    const deleteDialog = this.dialog.open(ConfirmdialogComponent, {
      data: buyer
    });
    deleteDialog.afterClosed().subscribe(result => {
      this.loadingPage();
    });
  }
}
