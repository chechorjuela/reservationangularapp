import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BuyerComponent} from './components/buyerComponent/buyer.component';
import {BallotComponent} from './components/ballotComponent/ballot.component';
import {ReservationComponent} from './components/reservationcomponent/reservation.component';

const routes: Routes = [
  {path: 'buyer', component: BuyerComponent},
  {path: 'ballot', component: BallotComponent},
  {path: 'reservation', component: ReservationComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
