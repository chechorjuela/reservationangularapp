import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './components/appComponent/app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BuyerComponent} from './components/buyerComponent/buyer.component';
import {BallotComponent} from './components/ballotComponent/ballot.component';
import {AppSettings} from './Setting/app.setting';
import {BuyerRepository, IBuyerRepository} from './Infrastructure/repository/buyer.repository';
import {BuyerService, IBuyerService} from './Domain/Services/buyer.service';
import {HttpClientModule} from '@angular/common/http';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {ModalbuyerComponent} from './components/modals/modal-buyer/modalbuyer.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import {MatCardModule} from '@angular/material/card';
import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ConfirmdialogComponent} from './components/modals/confirmDialog/Buyer/confirmdialog.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {ReservationComponent} from './components/reservationcomponent/reservation.component';
import {BallotRepository, IBallotRepository} from './Infrastructure/repository/ballot.repository';
import {BallotService, IBallotService} from './Domain/Services/ballot.service';
import {IReservationRepository, ReservationRepository} from './Infrastructure/repository/reservation.repository';
import {IReservationService, ReservationService} from './Domain/Services/reservation.service';
import {ModalballotComponent} from './components/modals/modalballot/modalballot.component';
import {ModalreservationComponent} from './components/modals/modalreservation/modalreservation.component';
import {ComfirmodalBallotComponent} from './components/modals/confirmDialog/ballot/confirmdialog.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {DatePipe} from '@angular/common';
import {ConfirmReservationdialogComponent} from './components/modals/confirmDialog/reservation/confirmReservationdialog.component';
import {MatSelectModule} from '@angular/material/select';

@NgModule({
  declarations: [
    AppComponent,
    BuyerComponent,
    BallotComponent,
    ModalbuyerComponent,
    ConfirmdialogComponent,
    ComfirmodalBallotComponent,
    ConfirmReservationdialogComponent,
    ReservationComponent,
    ModalballotComponent,
    ModalreservationComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatCardModule,
    ReactiveFormsModule,
    FormsModule,
    MatToolbarModule,
    MatListModule,
    MatSidenavModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
  ],
  providers: [
    AppSettings,
    DatePipe,
    {provide: IBuyerRepository, useClass: BuyerRepository},
    {provide: IBuyerService, useClass: BuyerService},
    {provide: IBallotRepository, useClass: BallotRepository},
    {provide: IBallotService, useClass: BallotService},
    {provide: IReservationRepository, useClass: ReservationRepository},
    {provide: IReservationService, useClass: ReservationService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
