import {Injectable} from '@angular/core';
import {BuyerRepository} from '../../Infrastructure/repository/buyer.repository';

export abstract class IBuyerService {
  abstract getBuyerAll(): Promise<any>;

  abstract saveBuyer(buyer: any): Promise<any>;

  abstract updateBuyer(buyer: any): Promise<any>;

  abstract deleteBuyer(buyer: any): Promise<any>;
}

@Injectable()
export class BuyerService implements IBuyerService {
  constructor(public buyerRepo: BuyerRepository) {
  }

  getBuyerAll(): Promise<any> {
    return this.buyerRepo.GetBuyerAll().then(data => {
      return data;
    });
  }

  saveBuyer(buyer: any): Promise<any> {
    return this.buyerRepo.CreateBuyer(buyer).then(response => {
      return response;
    });
  }

  deleteBuyer(buyer: any): Promise<any> {
    return this.buyerRepo.deleteBuyer(buyer).then(response => {
      return response;
    });
  }

  updateBuyer(buyer: any): Promise<any> {
    return this.buyerRepo.UpdateBuyer(buyer).then(response => {
      return response;
    });
  }
}
