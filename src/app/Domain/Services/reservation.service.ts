import {Injectable} from '@angular/core';
import {ReservationRepository} from '../../Infrastructure/repository/reservation.repository';

export abstract class IReservationService {
  abstract getReservationAll(): Promise<any>;

  abstract saveReservation(Reservation: any): Promise<any>;

  abstract updateReservation(Reservation: any): Promise<any>;

  abstract deleteReservation(Reservation: any): Promise<any>;
}

@Injectable()
export class ReservationService implements IReservationService {
  constructor(public ReservationRepo: ReservationRepository) {
  }

  getReservationAll(): Promise<any> {
    return this.ReservationRepo.GetReservationAll().then(data => {
      return data;
    });
  }

  saveReservation(Reservation: any): Promise<any> {
    return this.ReservationRepo.CreateReservation(Reservation).then(response => {
      return response;
    });
  }

  deleteReservation(Reservation: any): Promise<any> {
    return this.ReservationRepo.deleteReservation(Reservation).then(response => {
      return response;
    });
  }

  updateReservation(Reservation: any): Promise<any> {
    return this.ReservationRepo.UpdateReservation(Reservation).then(response => {
      return response;
    });
  }
}
