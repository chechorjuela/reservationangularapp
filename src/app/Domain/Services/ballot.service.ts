import {Injectable} from '@angular/core';
import {BallotRepository} from '../../Infrastructure/repository/ballot.repository';

export abstract class IBallotService {
  abstract getBallotAll(): Promise<any>;

  abstract saveBallot(Ballot: any): Promise<any>;

  abstract updateBallot(Ballot: any): Promise<any>;

  abstract deleteBallot(Ballot: any): Promise<any>;
}

@Injectable()
export class BallotService implements IBallotService {
  constructor(public BallotRepo: BallotRepository) {
  }

  getBallotAll(): Promise<any> {
    return this.BallotRepo.GetBallotAll().then(data => {
      return data;
    });
  }

  saveBallot(Ballot: any): Promise<any> {
    return this.BallotRepo.CreateBallot(Ballot).then(response => {
      return response;
    });
  }

  deleteBallot(Ballot: any): Promise<any> {
    return this.BallotRepo.deleteBallot(Ballot).then(response => {
      return response;
    });
  }

  updateBallot(Ballot: any): Promise<any> {
    return this.BallotRepo.UpdateBallot(Ballot).then(response => {
      return response;
    });
  }
}
